import os

ABSOLUTE_DIRPATH = os.path.dirname(__file__)
SAVED_MODELS_PATH = os.path.join(ABSOLUTE_DIRPATH, 'saved_models')
SAVED_TEST_RESULTS_PATH = os.path.join(ABSOLUTE_DIRPATH, 'test_results')
DATA_PATH = os.path.join(ABSOLUTE_DIRPATH, 'data')
