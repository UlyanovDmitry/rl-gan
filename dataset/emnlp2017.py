import json
import numpy as np
import os
from torch.utils.data import Dataset, DataLoader


class EMNLP2017Dataset(Dataset):
    def __init__(self, voc, dataset_type='train', max_len=256, path='./data/emnlp2017'):
        super().__init__()
        self.voc = voc
        self.max_len = max_len

        filename = dataset_type
        with open(os.path.join(path, f'{filename}.json')) as file:
            data = json.load(file)
            self.texts = [d['s'] for d in data]

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        text = self.texts[idx]
        tokens = self.voc.text2ids(text)[:self.max_len]
        # tokens = tokens + [self.voc.PAD_IDX for _ in range(self.max_len - len(tokens))]
        t = np.array(tokens, dtype=np.long)
        return t, text
