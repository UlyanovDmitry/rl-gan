import os
import numpy as np
from torch.utils.data import Dataset, DataLoader


class GigawordDataset(Dataset):
    def __init__(self, a_voc, t_voc, dataset_type='train', max_a_len=256, max_t_len=64, path='./data/gigaword'):
        super().__init__()
        self.a_voc = a_voc
        self.t_voc = t_voc
        self.max_a_len = max_a_len
        self.max_t_len = max_t_len

        if dataset_type == 'train':
            filename = 'train'
        elif dataset_type == 'valid':
            filename = 'dev'
        elif dataset_type == 'test':
            filename = 'test'
        else:
            raise Exception(f'Unknown dataset type: "{dataset_type}"')
        
        with open(os.path.join(path, f'{filename}.src.txt')) as file:
            articles = [a.replace('\n', '') for a in file.readlines()]
        with open(os.path.join(path, f'{filename}.tgt.txt')) as file:
            titles = [t.replace('\n', '') for t in file.readlines()]
        
        self.a = articles
        self.t = titles
    
    def __len__(self):
        return len(self.a)
        
    def __getitem__(self, idx):
        a_tokens = self.a_voc.text2ids(self.a[idx])[:self.max_a_len]
        t_tokens = self.a_voc.text2ids(self.t[idx])[:self.max_t_len]
        
        a = np.array(a_tokens, dtype=np.long)
        t = np.array(t_tokens, dtype=np.long)
        
        return a, t
