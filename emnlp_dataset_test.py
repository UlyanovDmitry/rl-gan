from dataset.emnlp2017 import EMNLP2017Dataset
from vocabulary.symbol_vocabulary import ReducedSymbolVocabulary

voc = ReducedSymbolVocabulary()
tdata = EMNLP2017Dataset(voc, 'train')

print(tdata[0])
print(''.join(voc.ids2text(tdata[0][0])))
print(len(tdata[0][0]))

