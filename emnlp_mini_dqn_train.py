from dataset import EMNLP2017Dataset
from vocabulary import ReducedSymbolVocabulary
from models.unconditional.dqn import G, D, VariationalEncoder
from trainers import UnconditionalTrainer
import torch.optim as optim
import config
import torch
import os
import pickle
from datetime import datetime

if __name__ == '__main__':
    BS = 32
    N_STEPS = 200001
    MAX_LEN = 32
    STEPS_PER_DISPLAY = 50

    voc = ReducedSymbolVocabulary()

    D_S = 512
    D_EMB = 32
    D_ACT = len(voc)

    EPS_START = 0.9
    EPS_END = 0.01
    EPS_DECAY = 100

    d = D(D_EMB, D_S, D_S, D_ACT).cuda()
    g = G(D_S, D_S, D_ACT, MAX_LEN).cuda()
    e = VariationalEncoder(D_EMB, D_S, D_S, D_ACT).cuda()

    env_name = 'Unconditioned EMNLP2017 DQN (2020.06.14) 512-32, max_len=32, symbol lvl 3'

    d_opt = optim.Adam(d.parameters(), lr=1e-4)
    g_opt = optim.RMSprop(g.parameters(), lr=1e-3)
    e_opt = optim.Adam(e.parameters())
    try:
        d.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'd.pth')))
        g.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'g.pth')))
        e.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'e.pth')))
        d_opt.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'd_opt.pth')))
        g_opt.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'g_opt.pth')))
        e_opt.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'e_opt.pth')))
    except Exception as ex:
        print(ex)
        # raise ex

    try:
        with open(os.path.join(config.SAVED_MODELS_PATH, env_name, 'steps.pickle'), 'rb') as file:
                epoch, step = pickle.load(file)
    except Exception as ex:
        print("couldn't read steps!")
        epoch, step = 0, 0
        # raise ex

    try:
        with open(os.path.join(config.SAVED_MODELS_PATH, env_name, 'best_scores.pickle'), 'rb') as file:
                best_scores = pickle.load(file)
    except Exception as ex:
        print("couldn't read scores!")
        best_scores = {'fed': None}

    train_dataset = EMNLP2017Dataset(voc, 'train', MAX_LEN)
    valid_dataset = EMNLP2017Dataset(voc, 'valid', MAX_LEN)
    valid_dataset.texts = valid_dataset.texts[:1000]

    trainer = UnconditionalTrainer(env_name, voc, train_dataset, valid_dataset)
    print(datetime.now())
    trainer.train(g, d, e,
                  g_opt, d_opt, e_opt,
                  BS, epoch=epoch, step=step + 1, best_scores=best_scores, max_step=N_STEPS)
