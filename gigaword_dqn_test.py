from dataset import GigawordDataset
from vocabulary import ReducedSymbolVocabulary
from models.summarization.dqn import G, D
from trainers import SummarizationTrainer
import torch.optim as optim
import config
import torch
import os
import pickle
import pandas as pd
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import PackedSequence, pack_sequence, pad_sequence, pad_packed_sequence
from utils import fid
import pickle
from rouge_score import rouge_scorer
from rouge_score import scoring
import config

if __name__ == '__main__':
    BS = 64
    N_STEPS = 200000
    MAX_LEN = 32
    STEPS_PER_DISPLAY = 50

    voc = ReducedSymbolVocabulary()

    D_S = 512
    D_EMB = 32
    D_ACT = len(voc)

    EPS_START = 0.9
    EPS_END = 0.01
    EPS_DECAY = 100

    d = D(D_EMB, D_S, D_ACT).cuda()
    g = G(D_S, D_ACT, MAX_LEN).cuda()

    env_name = 'Summarization Gigaword DQN (2020.05.23) 512-32, max_len=32, symbol lvl'

    d.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'best_rouge2', 'd.pth')))
    g.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'best_rouge2', 'g.pth')))

    test_dataset = GigawordDataset(voc, voc, 'test', max_t_len=32)

    # valid_dataset = GigawordDataset(voc, voc, 'valid', max_t_len=32)
    # valid_dataset.a = valid_dataset.a[:1000]
    # valid_dataset.t = valid_dataset.t[:1000]



    def custom_collate_fn(samples):
        articles = [torch.from_numpy(s[0]).long() for s in samples]
        titles = pad_sequence([torch.from_numpy(s[1]).long() for s in samples],
                              batch_first=True, padding_value=voc.PAD_IDX)
        return articles, titles


    scorer = rouge_scorer.RougeScorer(['rouge1', 'rouge2', 'rougeL'], use_stemmer=True)
    aggregator = scoring.BootstrapAggregator()
    test_dataloader = DataLoader(test_dataset, batch_size=BS, shuffle=False,
                                 collate_fn=custom_collate_fn)

    articles = []
    titles = []
    generated_titles = []

    with torch.no_grad():
        for (art, ttl) in test_dataloader:
            act_bs = ttl.size(0)
            ttl = ttl.cuda()
            _, _, gen, _ = g(pack_sequence(art, enforce_sorted=False).cuda(), d, eps_threshold=0.0)  # may be 0
            for i in range(act_bs):
                art_text = ''.join(voc.ids2text(art[i], return_special=False))
                ttl_text = ''.join(voc.ids2text(ttl[i], return_special=False))
                gen_text = ''.join(voc.ids2text(gen[i], return_special=False))
                aggregator.add_scores(scorer.score(gen_text, ttl_text))

                articles.append(art_text)
                titles.append(ttl_text)
                generated_titles.append(gen_text)
    rouge_score = aggregator.aggregate()
    fid_score = fid(generated_titles, titles)

    examples = pd.DataFrame(data={'articles': articles,
                                  'titles': titles,
                                  'generated': generated_titles})
    examples.to_csv(os.path.join(config.SAVED_TEST_RESULTS_PATH, env_name, 'examples.csv'))

    print(rouge_score)

    print(rouge_score)

    print(fid_score)

