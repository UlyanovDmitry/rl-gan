from dataset import GigawordDataset
from vocabulary import ReducedSymbolVocabulary
from models.summarization.curriculum_multi_d_ms_dqn import G, D
from trainers import CurriculumMsSummarizationTrainer
import torch.optim as optim
import config
import torch
import os
import pickle

if __name__ == '__main__':
    BS = 64
    N_STEPS = 200001
    MAX_LEN = 32
    STEPS_PER_DISPLAY = 50

    voc = ReducedSymbolVocabulary()

    D_S = 512
    D_EMB = 32
    D_ACT = len(voc)

    EPS_START = 0.9
    EPS_END = 0.01
    EPS_DECAY = 100

    d = D(D_EMB, D_S, D_ACT, MAX_LEN).cuda()
    g = G(D_S, D_ACT, MAX_LEN).cuda()

    env_name = 'Summarization Gigaword MultiD DQN MS CURRICULUM (2020.06.13) 512-32, max_len=32, symbol lvl'

    d_opt = optim.Adam(d.parameters(), lr=1e-4)
    g_opt = optim.RMSprop(g.parameters(), lr=1e-3)
    try:
        d.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'd.pth')))
        g.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'g.pth')))
        d_opt.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'd_opt.pth')))
        g_opt.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'current', 'g_opt.pth')))
    except Exception as ex:
        print(ex)

    try:
        with open(os.path.join(config.SAVED_MODELS_PATH, env_name, 'steps.pickle'), 'rb') as file:
                epoch, step = pickle.load(file)
    except Exception as ex:
        print(ex)
        epoch, step = 0, 0

    # step = 95000
    print(epoch, step)

    try:
        with open(os.path.join(config.SAVED_MODELS_PATH, env_name, 'best_scores.pickle'), 'rb') as file:
                best_scores = pickle.load(file)
    except Exception as ex:
        best_scores = {'rouge1': None, 'rouge2': None, 'rougeL': None, 'fed': None}

    train_dataset = GigawordDataset(voc, voc, 'train', max_t_len=32)
    valid_dataset = GigawordDataset(voc, voc, 'valid', max_t_len=32)
    valid_dataset.a = valid_dataset.a[:1000]
    valid_dataset.t = valid_dataset.t[:1000]

    trainer = CurriculumMsSummarizationTrainer(env_name, voc, train_dataset, valid_dataset)
    trainer.train(g, d,
                  g_opt, d_opt,
                  BS, epoch=epoch, step=step + 1, best_scores=best_scores, max_step=N_STEPS)
