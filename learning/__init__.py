import torch
import torch.nn as nn
import abc
import os
import pickle


class ISummaryWriter:
    @abc.abstractmethod
    def update(self, **kwargs):
        raise NotImplemented

    @abc.abstractmethod
    def load_state(self, path):
        raise NotImplemented

    @abc.abstractmethod
    def save_state(self, path):
        raise NotImplemented


class ILearner:
    def __init__(self, g, d,
                 g_opt, d_opt,
                 voc, save_path):
        self.g = g
        self.d = d
        self.g_opt = g_opt
        self.d_opt = d_opt
        self.save_path = save_path
        self.voc = voc

    def load_models(self, path):
        self.d.load_state_dict(torch.load(os.path.join(path, 'd.pth')))
        self.g.load_state_dict(torch.load(os.path.join(path, 'g.pth')))
        self.d_opt.load_state_dict(torch.load(os.path.join(path, 'd_opt.pth')))
        self.g_opt.load_state_dict(torch.load(os.path.join(path, 'g_opt.pth')))

    def save_models(self, path):
        torch.save(self.d.state_dict(), os.path.join(path, 'd.pth'))
        torch.save(self.g.state_dict(), os.path.join(path, 'g.pth'))
        torch.save(self.d_opt.state_dict(), os.path.join(path, 'd_opt.pth'))
        torch.save(self.g_opt.state_dict(), os.path.join(path, 'g_opt.pth'))

    @abc.abstractmethod
    def load(self):
        raise NotImplemented

    @abc.abstractmethod
    def save(self):
        raise NotImplemented

    @abc.abstractmethod
    def validation(self, valid_loader):
        raise NotImplemented

    @abc.abstractmethod
    def train_step(self, batch):
        raise NotImplemented

    @abc.abstractmethod
    def custom_collate_fn(self, samples):
        raise NotImplemented

    def train_epoch(self, train_loader):
        class _train_generator:
            def __init__(self, train_step_fun):
                self.train_step_fun = train_step_fun

            def __len__(self):
                return len(train_loader)

            def __iter__(self):
                for batch in train_loader:
                    yield self.train_step_fun(batch)

        return _train_generator(self.train_step)
