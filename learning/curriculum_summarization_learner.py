import math
import pickle

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import PackedSequence, pack_sequence, pad_sequence, pad_packed_sequence
from learning import ILearner
from rouge_score import rouge_scorer
from rouge_score import scoring
import os
from utils import fid, get_accuracy
import pandas as pd


class CurriculumSummarizationLearner(ILearner):
    def __init__(self, g, d, g_opt, d_opt,
                 voc, save_path, max_bs, max_t_len,
                 eps_start, eps_end, eps_decay,
                 epoch=0, step=0):
        super().__init__(g, d, g_opt, d_opt, voc, save_path)
        self.eps_start = eps_start
        self.eps_end = eps_end
        self.eps_decay = eps_decay
        self.epoch = epoch
        self.step = step

        self.d_loss_fn = nn.BCEWithLogitsLoss()
        self.g_loss_fn = nn.SmoothL1Loss()

        self.best_scores = {'rouge1': None, 'rouge2': None, 'rougeL': None, 'fed': None}

        # mask init
        self.g_mask = torch.ones(max_bs, max_t_len).cuda()
        for i in range(max_bs):
            self.g_mask[i, :int(i / max_bs * max_t_len)] = 0

        self.g_mask_i = self.g_mask.int()

        self.d_mask = torch.ones(max_bs, max_t_len).cuda()
        self.d_mask_i = self.d_mask.int()
        self.dropout_w = max_t_len / self.d_mask.sum(-1)

    def custom_collate_fn(self, samples):
        articles = [torch.from_numpy(s[0]).long() for s in samples]
        titles = pad_sequence([torch.from_numpy(s[1]).long() for s in samples],
                              batch_first=True, padding_value=self.voc.PAD_IDX)
        return articles, titles

    def load(self):
        self.load_models(os.path.join(self.save_path, 'current'))
        with open(os.path.join(self.save_path, 'steps.pickle'), 'rb') as file:
            self.epoch, self.step = pickle.load(file)

        with open(os.path.join(self.save_path, 'best_scores.pickle'), 'rb') as file:
            self.best_scores = pickle.load(file)

    def save(self):
        cur_model_save_path = os.path.join(self.save_path, 'current')
        if not os.path.exists(cur_model_save_path):
            os.mkdir(cur_model_save_path)
        self.save_models(cur_model_save_path)

        with open(os.path.join(self.save_path, 'steps.pickle'), 'wb') as file:
            pickle.dump((self.epoch, self.step), file)

        with open(os.path.join(self.save_path, 'best_scores.pickle'), 'wb') as file:
            pickle.dump(self.best_scores, file)

    def validation(self, valid_loader):
        scorer = rouge_scorer.RougeScorer(['rouge1', 'rouge2', 'rougeL'], use_stemmer=True)
        aggregator = scoring.BootstrapAggregator()

        articles = []
        titles = []
        generated_titles = []

        v_mask = torch.ones(valid_loader.batch_size, self.g.max_steps).int().cuda()

        with torch.no_grad():
            for (art, ttl) in valid_loader:
                act_bs = ttl.size(0)
                ttl = ttl.cuda()
                _, _, gen, _ = self.g(pack_sequence(art, enforce_sorted=False).cuda(), ttl,
                                      v_mask[:act_bs], self.d, eps_threshold=0)
                for i in range(act_bs):
                    art_text = ''.join(self.voc.ids2text(art[i], return_special=False))
                    ttl_text = ''.join(self.voc.ids2text(ttl[i], return_special=False))
                    gen_text = ''.join(self.voc.ids2text(gen[i], return_special=False))
                    aggregator.add_scores(scorer.score(gen_text, ttl_text))

                    articles.append(art_text)
                    titles.append(ttl_text)
                    generated_titles.append(gen_text)
        rouge_score = aggregator.aggregate()
        fid_score = fid(generated_titles, titles)
        examples = pd.DataFrame(data={'articles': articles,
                                      'titles': titles,
                                      'generated': generated_titles})

        if self.best_scores['rouge1'] is None or self.best_scores['rouge1'] < rouge_score['rouge1'].mid.fmeasure:
            self.best_scores['rouge1'] = rouge_score['rouge1'].mid.fmeasure
            path = os.path.join(self.save_path, 'best_rouge1')
            if not os.path.exists(path):
                os.mkdir(path)
            self.save_models(path)
            self.save()
            examples.to_csv(os.path.join(path, 'examples.csv'))
        if self.best_scores['rouge2'] is None or self.best_scores['rouge2'] < rouge_score['rouge2'].mid.fmeasure:
            self.best_scores['rouge2'] = rouge_score['rouge2'].mid.fmeasure
            path = os.path.join(self.save_path, 'best_rouge2')
            if not os.path.exists(path):
                os.mkdir(path)
            self.save_models(path)
            self.save()
            examples.to_csv(os.path.join(path, 'examples.csv'))
        if self.best_scores['rougeL'] is None or self.best_scores['rougeL'] < rouge_score['rougeL'].mid.fmeasure:
            self.best_scores['rougeL'] = rouge_score['rougeL'].mid.fmeasure
            path = os.path.join(self.save_path, 'best_rougeL')
            if not os.path.exists(path):
                os.mkdir(path)
            self.save_models(path)
            self.save()
            examples.to_csv(os.path.join(path, 'examples.csv'))
        if self.best_scores['fed'] is None or self.best_scores['fed'] > fid_score:
            self.best_scores['fed'] = fid_score
            path = os.path.join(self.save_path, 'best_fed')
            if not os.path.exists(path):
                os.mkdir(path)
            self.save_models(path)
            self.save()
            examples.to_csv(os.path.join(path, 'examples.csv'))

        return rouge_score, fid_score

    def train_step(self, batch):
        articles, titles = batch
        full_bs = titles.size(0)
        if full_bs % 3 != 0:
            return None
        act_bs = full_bs // 3

        dra = pack_sequence(articles[:act_bs], enforce_sorted=False).cuda()
        drt = titles[:act_bs].cuda()
        dfa = articles[act_bs: 2 * act_bs]  # , enforce_sorted=False).cuda()
        dft = titles[act_bs: 2 * act_bs].cuda()
        gra = articles[2 * act_bs:]
        grt = titles[2 * act_bs:].cuda()

        eps_threshold = self.eps_end + (self.eps_start - self.eps_end) * math.exp(-1. * self.step / self.eps_decay)

        # d train step
        with torch.no_grad():
            _, _, fake_titles, _ = self.g(pack_sequence(dfa, enforce_sorted=False).cuda(), dft,
                                          self.d_mask_i[:dft.size(0)], self.d, eps_threshold=0.0)
        self.d_opt.zero_grad()
        scores_real = self.d(dra, drt)
        r_real = scores_real.sum(-1)
        scores_fake = self.d(pack_sequence(dfa, enforce_sorted=False).cuda(), fake_titles)
        r_fake = (scores_fake * self.d_mask).sum(-1) * self.dropout_w
        d_loss = (self.d_loss_fn(r_real, torch.ones_like(r_real)) +
                  self.d_loss_fn(r_fake, torch.zeros_like(r_fake))) / 2
        d_loss.backward()
        self.d_opt.step()
        res_d_loss = d_loss.cpu().detach().item()
        t = torch.cat([r_real.clone().fill_(1.0),
                       r_fake.clone().fill_(0.0)], dim=0).view(-1).cpu().detach().tolist()
        p = torch.cat([r_real.sigmoid(),
                       r_fake.sigmoid()], dim=0).view(-1).cpu().detach().tolist()

        acc, d_acc = get_accuracy(t, p, 0.5)

        real_rewards = r_real.cpu().detach().tolist()
        fake_rewards = r_fake.cpu().detach().tolist()

        # g train step
        self.g_opt.zero_grad()
        q, r, a, t = self.g(pack_sequence(gra, enforce_sorted=False).cuda(), grt,
                            self.g_mask_i[:grt.size(0)], self.d, eps_threshold=eps_threshold)
        g_loss = self.g_loss_fn(q, t.detach())
        g_loss.backward()
        torch.nn.utils.clip_grad_norm_(self.g.parameters(), 1.0)
        self.g_opt.step()
        res_g_loss = g_loss.cpu().detach().item()

        self.step += 1

        return (res_d_loss, res_g_loss), (acc, d_acc), (real_rewards, fake_rewards)

        # # get best example
        # with torch.no_grad():
        #     i_max = r_fake.argmax()
        #     if best_example[0] is None or best_example[0] < r_fake[i_max]:
        #         best_example = (r_fake[i_max].cpu(), dfa[i_max].cpu(),
        #                         dft[i_max].cpu(), fake_titles[i_max].cpu(),
        #                         d(pack_sequence(dfa[i_max:i_max + 1], enforce_sorted=False).cuda(),
        #                           dft[i_max:i_max + 1])[0].cpu(),
        #                         scores_fake[i_max].cpu())
