import math
import pickle

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import PackedSequence, pack_sequence, pad_sequence, pad_packed_sequence
from learning import ILearner
from rouge_score import rouge_scorer
from rouge_score import scoring
import os
from utils import fid, get_accuracy
import pandas as pd


class UnconditionalLearner(ILearner):
    def __init__(self, g, d, e,
                 g_opt, d_opt, e_opt,
                 voc, save_path,
                 eps_start, eps_end, eps_decay,
                 epoch=0, step=0):
        super().__init__(g, d, g_opt, d_opt, voc, save_path)
        self.e = e
        self.e_opt = e_opt
        self.eps_start = eps_start
        self.eps_end = eps_end
        self.eps_decay = eps_decay
        self.epoch = epoch
        self.step = step
        self.max_len = g.max_steps
        self.d_loss_fn = nn.BCEWithLogitsLoss()
        self.g_loss_fn = nn.SmoothL1Loss()

        self.best_scores = {'fed': None}

    def custom_collate_fn(self, samples):
        seq = [s[0] for s in samples]
        raw_texts = [s[1] for s in samples]
        bs = len(seq)
        tensor_seq = [torch.from_numpy(seq[i]).long() for i in range(bs)]
        packed_texts = pack_sequence(tensor_seq, enforce_sorted=False)  # for encoder
        padded_texts = torch.full(size=(bs, self.max_len), fill_value=self.voc.PAD_IDX, dtype=torch.long)# for discriminator
        for i in range(bs):
            padded_texts[i, :len(seq[i])] = tensor_seq[i]
        return packed_texts, padded_texts, raw_texts

    def load(self):
        self.load_models(os.path.join(self.save_path, 'current'))
        self.e.load_state_dict(torch.load(os.path.join(self.save_path, 'current', 'e.pth')))
        self.e_opt.load_state_dict(torch.load(os.path.join(self.save_path, 'current', 'e_opt.pth')))
        with open(os.path.join(self.save_path, 'steps.pickle'), 'rb') as file:
            self.epoch, self.step = pickle.load(file)

    def save(self):
        cur_model_save_path = os.path.join(self.save_path, 'current')
        if not os.path.exists(cur_model_save_path):
            os.mkdir(cur_model_save_path)
        self.save_models(cur_model_save_path)
        torch.save(self.e.state_dict(), os.path.join(cur_model_save_path, 'e.pth'))
        torch.save(self.e_opt.state_dict(), os.path.join(cur_model_save_path, 'e_opt.pth'))

        with open(os.path.join(self.save_path, 'steps.pickle'), 'wb') as file:
            pickle.dump((self.epoch, self.step), file)

    def validation(self, valid_loader):
        real_texts = []
        generated_texts = []

        with torch.no_grad():
            for (packed_texts, padded_texts, samples) in valid_loader:
                real_texts += samples

                act_bs = padded_texts.size(0)
                _, _, gen, _, _ = self.g(act_bs, self.d, eps_threshold=0.0)
                for i in range(act_bs):
                    gen_text = ''.join(self.voc.ids2text(gen[i], return_special=False))
                    generated_texts.append(gen_text)

        fid_score = fid(generated_texts, real_texts)
        examples = pd.DataFrame(data={'examples': generated_texts})

        if self.best_scores['fed'] is None or self.best_scores['fed'] > fid_score:
            self.best_scores['fed'] = fid_score
            path = os.path.join(self.save_path, 'best_fed')
            if not os.path.exists(path):
                os.mkdir(path)
            self.save_models(path)
            self.save()
            examples.to_csv(os.path.join(path, 'examples.csv'))

        return fid_score

    def train_step(self, batch):
        packed_texts, padded_texts, samples = batch

        padded_texts = padded_texts.cuda()
        eps_threshold = self.eps_end + (self.eps_start - self.eps_end) * math.exp(-1. * self.step / self.eps_decay)
        act_bs = padded_texts.size(0)

        ##  e/d train step
        self.e_opt.zero_grad()
        self.d_opt.zero_grad()

        with torch.no_grad():
            _, _, fake_texts, _, z_fake = self.g(act_bs, self.d, eps_threshold=0.0)  # may be 0
        scores_fake = self.d(z_fake, fake_texts)
        r_fake = scores_fake.sum(-1)

        z_real, (mu, sigma) = self.e(packed_texts)
        scores_real = self.d(z_real, padded_texts)
        r_real = scores_real.sum(-1)
        d_loss = (self.d_loss_fn(r_real, torch.ones_like(r_real)) +
                  self.d_loss_fn(r_fake, torch.zeros_like(r_fake))) / 2
        e_kld = 10 * -0.5 * (1 + 2 * sigma.log() - mu.pow(2) - sigma.pow(2)).sum()

        d_loss.backward(retain_graph=True)
        self.d_opt.step()

        e_kld.backward()
        self.e_opt.step()

        res_e_loss = e_kld.cpu().detach().item()
        res_d_loss = d_loss.cpu().detach().item()
        t = torch.cat([r_real.clone().fill_(1.0),
                       r_fake.clone().fill_(0.0)], dim=0).view(-1).cpu().detach().tolist()
        p = torch.cat([r_real.sigmoid(),
                       r_fake.sigmoid()], dim=0).view(-1).cpu().detach().tolist()
        acc, d_acc = get_accuracy(t, p, 0.5)
        real_rewards = r_real.cpu().detach().tolist()
        fake_rewards = r_fake.cpu().detach().tolist()

        # g train step
        self.g_opt.zero_grad()
        q, r, a, t, _ = self.g(act_bs, self.d, eps_threshold=eps_threshold)
        g_loss = self.g_loss_fn(q, t.detach())
        g_loss.backward()
        torch.nn.utils.clip_grad_norm_(self.g.parameters(), 1.0)
        self.g_opt.step()
        res_g_loss = g_loss.cpu().detach().item()

        self.step += 1

        return (res_d_loss, res_g_loss), (acc, d_acc), (real_rewards, fake_rewards)
