import torch
import torch.nn as nn
from torch.nn.utils.rnn import PackedSequence


class D(nn.Module):
    def __init__(self, d_emb, d_h, vocab_size):
        super().__init__()
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)

        self.arnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.h_0 = nn.Parameter(torch.randn(1, 1, d_h), requires_grad=True)

        self.rnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.reward_fn = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=d_h),
            nn.ReLU(),
            nn.Linear(in_features=d_h, out_features=1))

        self._lx = nn.Linear(in_features=d_emb, out_features=3 * d_h, bias=True)
        self._lh = nn.Linear(in_features=d_h, out_features=3 * d_h, bias=True)
        self._lx.weight.data = self.rnn.weight_ih_l0.data
        self._lx.bias.data = self.rnn.bias_ih_l0.data
        self._lh.weight.data = self.rnn.weight_hh_l0.data
        self._lh.bias.data = self.rnn.bias_hh_l0.data

    def encode(self, articles):
        bs = articles.batch_sizes[0]
        embedded = self.embeddings(articles.data)
        embedded = PackedSequence(data=embedded,
                                  batch_sizes=articles.batch_sizes,
                                  sorted_indices=articles.sorted_indices,
                                  unsorted_indices=articles.unsorted_indices)
        _, h_n = self.arnn(embedded, self.h_0.expand(-1, bs, -1))
        return h_n  # [1, bs, d_h]

    def forward(self, articles, titles):
        h0 = self.encode(articles)
        embedded = self.embeddings(titles)
        h, _ = self.rnn(embedded, h0)
        r = self.reward_fn(h).squeeze(-1)
        return r

    def get_initial_state(self, articles):
        self._lx.weight.data = self.rnn.weight_ih_l0.data
        self._lx.bias.data = self.rnn.bias_ih_l0.data
        self._lh.weight.data = self.rnn.weight_hh_l0.data
        self._lh.bias.data = self.rnn.bias_hh_l0.data
        return self.encode(articles).squeeze(0)  # [bs, d_h]

    def step(self, s_t, a_t):
        s_t = s_t.unsqueeze(0)
        a_t = a_t.unsqueeze(1)

        embedded = self.embeddings(a_t)
        _, h = self.rnn(embedded, s_t)
        h = h.squeeze(0)
        r = self.reward_fn(h).view(-1)

        return r, h


class G(nn.Module):
    def __init__(self, d_state, n_actions, max_steps=32, decay_factor=0.99):
        super().__init__()
        self.d_state = d_state
        self.n_actions = n_actions
        self.max_steps = max_steps
        self.decay_factor = decay_factor
        self.body = nn.Sequential(
            nn.Linear(d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 1)
        )

    def forward(self, articles, titles, mask, env, eps_threshold=0.0):
        bs = articles.batch_sizes[0]

        v = []  ## v-values list
        r = []  ## rewards list
        a = []  ## actions list
        t = []  ## targets list

        with torch.no_grad():
            s_t = env.get_initial_state(articles).unsqueeze(1)  # [bs, 1, d_state]
            all_actions = env.embeddings.weight
            wx = env._lx(all_actions).unsqueeze(0).expand(bs, -1, -1)  # [bs, n_actions, 3 * d_s]

        for step in range(self.max_steps):
            with torch.no_grad():
                s_aug = s_t.expand(-1, self.n_actions, -1)  # [bs, n_actions, d_state]
                ws = env._lh(s_t)  # [bs, 1, 3 * d_state]
                ws = ws.expand(-1, self.n_actions, -1)  # [bs, n_actions, 3 * d_state]

                rx = wx[:, :, :self.d_state]
                zx = wx[:, :, self.d_state:2 * self.d_state]
                cx = wx[:, :, 2 * self.d_state:]

                rh = ws[:, :, :self.d_state]
                zh = ws[:, :, self.d_state:2 * self.d_state]
                ch = ws[:, :, 2 * self.d_state:]

                z_gate = (zx + zh).sigmoid()
                r_gate = (rx + rh).sigmoid()
                c = (cx + ch * r_gate).tanh()

                new_s = z_gate * s_aug + (1 - z_gate) * c  # [bs, n_actions, d_state]
                all_r = env.reward_fn(new_s).view(bs, -1)  # [bs, n_actions]
                all_v = self.body(new_s).view(bs, -1)  # [bs, n_actions]
                q = all_v + all_r  # [bs, n_actions]

                _, max_a = q.max(1)

                probs = torch.empty(bs, self.n_actions, device=q.device).fill_(
                    eps_threshold / self.n_actions)  # (self.n_actions - 1))
                p_mask = (torch.arange(self.n_actions, device=q.device).view(1, -1).repeat(bs, 1) == max_a.view(-1,
                                                                                                                1)).float()
                probs += p_mask * (1 - eps_threshold)
                a_t = torch.multinomial(probs, 1, True).view(-1)
                a_t = (a_t * mask[:, step] + titles[:, step] * (1 - mask[:, step]))
                max_a = (max_a * mask[:, step] + titles[:, step] * (1 - mask[:, step]))

                s_t = torch.gather(new_s, dim=1, index=a_t.view(-1, 1, 1).expand(-1, -1, self.d_state))
                r_t = torch.gather(all_r, dim=1, index=a_t.view(-1, 1)).view(-1)
                t_t = self.body(
                    torch.gather(new_s, dim=1, index=max_a.view(-1, 1, 1).expand(-1, -1, self.d_state))).view(-1)

            v_t = self.body(s_t).view(-1)

            v.append(v_t)
            r.append(r_t)
            a.append(a_t)
            t.append(t_t)

        v = torch.stack(v, dim=1)
        r = torch.stack(r, dim=1)
        a = torch.stack(a, dim=1)

        t = torch.stack(t, dim=1)

        t[:, :-1] = self.decay_factor * t[:, 1:]
        t[:, -1] = 0
        t += r

        return v, r, a, t
