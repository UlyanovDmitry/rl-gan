import random
import torch
import torch.nn as nn


class D(nn.Module):
    def __init__(self, d_emb, d_h, vocab_size):
        super().__init__()
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)
        self.arnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.trnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.reward_fn = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=d_h),
            nn.ReLU(),
            nn.Linear(in_features=d_h, out_features=1))
        self.h_0 = nn.Parameter(torch.randn(1, 1, d_h))

    def forward(self, articles, titles):
        r"""
        in:
            articles.size() == [bs, m_len]
            titles.size() == [bs, m_len]
        out:
            r.size() == [bs, m_len]
        """
        h0 = self.encode(articles)
        embedded = self.embeddings(titles)
        h, _ = self.trnn(embedded, h0)
        r = self.reward_fn(h).squeeze(-1)
        return r  # .sum(-1)

    def encode(self, text):
        bs = text.batch_sizes.max().item()
        encoded = nn.utils.rnn.PackedSequence(data=self.embeddings(text.data),
                                              batch_sizes=text.batch_sizes,
                                              sorted_indices=text.sorted_indices,
                                              unsorted_indices=text.unsorted_indices)
        _, h = self.arnn(encoded, self.h_0.expand(-1, bs, -1).contiguous())
        return h
    def step(self, s_t, a_t):
        r"""
        in:
            s_t.size() == [bs, d_h]
            a_t.size() == [bs]
        out:
            r_t.size() == [bs]
            s.size() == [bs, d_h]
        """
        bs = s_t.size(0)
        s_t = s_t.unsqueeze(0)
        a_t = a_t.unsqueeze(1)

        embedded = self.embeddings(a_t)
        h, _ = self.trnn(embedded, s_t.contiguous())
        r = self.reward_fn(h).squeeze(-1)

        return r.view(-1), h.squeeze(1)


class G(nn.Module):
    def __init__(self, d_state, n_actions, max_steps=32, decay_factor=0.99):
        super().__init__()
        self.d_state = d_state
        self.n_actions = n_actions
        self.max_steps = max_steps
        self.decay_factor = decay_factor
        self.body = nn.Sequential(
            nn.Linear(d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 1)
        )

    def sample_action(self, adv, eps_threshold):
        r"""
        in:
            adv.size() == [bs, n_actions]
        out:
            a.size() == [bs]
        """
        bs = adv.size(0)
        with torch.no_grad():
            max_actions = adv.max(dim=1)[1].view(-1)
            actions = max_actions.clone()
            for i in range(bs):
                sample = random.random()
                if sample < eps_threshold:
                    actions[i] = random.randrange(self.n_actions)
            return actions, max_actions

    #     def forward(self, articles, env, max_steps=32, eps_threshold=0.0):
    #         bs = articles.size(0)
    def forward(self, articles, titles, mask, env, eps_threshold=0.0):
        r"""
        texts.size() == [bs, max_len]
        mask.size() == [bs, max_len] 0 where should use real text
        """
        v = []  ## v-values list
        r = []  ## rewards list
        a = []  ## actions list
        t = []  ## targets list

        bs, seq_len = titles.size(0), titles.size(1)

        s_t = env.encode(articles).view(bs, -1)  # [bs, d_state]
        all_actions = torch.arange(self.n_actions).repeat(bs).to(s_t.device)  # [bs * n_actions]
        act_bias = torch.arange(bs).to(s_t.device) * self.n_actions

        #         vs_t = self.body(s_t).view(-1) # [bs,]
        #         v.append(vs_t)

        for step in range(seq_len):
            s_aug = s_t.repeat(1, self.n_actions).view(-1, self.d_state)  # [bs * n_actions, d_state]

            # evaluate candidates
            cr_t, cs_t = env.step(s_aug, all_actions)  # [bs * n_actions], [bs * n_actions, d_state]
            cvs_t = self.body(cs_t).view(-1)  # [bs * n_actions]

            # calculate advantage
            adv_t = (cvs_t + cr_t).view(bs, -1)  # [bs, n_actions]
            a_t, max_a_t = self.sample_action(adv_t, eps_threshold)  # [bs], [bs]

            a_t = (a_t * mask[:, step] + titles[:, step] * (1 - mask[:, step]))

            idx = a_t + act_bias
            vs_t = cvs_t[idx]  # [bs]
            r_t = cr_t[idx]  # [bs]
            s_t = cs_t[idx]  # [bs, d_state]

            v.append(vs_t)
            r.append(r_t)
            a.append(a_t)
            t.append(cvs_t[max_a_t + act_bias])

        v = torch.stack(v, dim=1)
        r = torch.stack(r, dim=1)
        a = torch.stack(a, dim=1)

        t = torch.stack(t, dim=1)

        t[:, :-1] = self.decay_factor * t[:, 1:]
        t[:, -1] = 0
        t += r

        return v, r, a, t
