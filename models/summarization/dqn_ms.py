import random
import torch
import torch.nn as nn


class D(nn.Module):
    def __init__(self, d_emb, d_h, vocab_size):
        super().__init__()
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)
        self.arnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.trnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.reward_fn = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=d_h),
            nn.ReLU(),
            nn.Linear(in_features=d_h, out_features=1))
        self.h_0 = nn.Parameter(torch.randn(1, 1, d_h), requires_grad=True)

    def forward(self, articles, titles):
        r"""
        in:
            articles.size() == [bs, m_len]
            titles.size() == [bs, m_len]
        out:
            r.size() == [bs, m_len]
        """
        h0 = self.encode(articles)
        embedded = self.embeddings(titles)
        h, _ = self.trnn(embedded, h0)
        r = self.reward_fn(h).squeeze(-1)
        return r  # .sum(-1)

    def encode(self, text):
        bs = text.batch_sizes.max().item()
        encoded = nn.utils.rnn.PackedSequence(data=self.embeddings(text.data),
                                              batch_sizes=text.batch_sizes,
                                              sorted_indices=text.sorted_indices,
                                              unsorted_indices=text.unsorted_indices)
        _, h = self.arnn(encoded, self.h_0.expand(-1, bs, -1).contiguous())
        return h

    def step(self, s_t, a_t):
        r"""
        in:
            s_t.size() == [bs, d_h]
            a_t.size() == [bs]
        out:
            r_t.size() == [bs]
            s.size() == [bs, d_h]
        """
        bs = s_t.size(0)
        s_t = s_t.unsqueeze(0)
        a_t = a_t.unsqueeze(1)

        embedded = self.embeddings(a_t)
        h, _ = self.trnn(embedded, s_t.contiguous())
        r = self.reward_fn(h).sigmoid().squeeze(-1)

        return r.view(-1), h.squeeze(1)


class G(nn.Module):
    def __init__(self, d_state, n_actions, max_steps=32, decay_factor=0.99):
        super().__init__()
        self.n_actions = n_actions
        self.max_steps = max_steps
        self.decay_factor = decay_factor
        self.body = nn.Sequential(
            nn.Linear(d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, n_actions)
        )

    def sample_action(self, q, eps_threshold):
        r"""
        in:
            q.size() == [bs, n_actions]
        out:
            a.size() == [bs]
        """
        bs = q.size(0)
        with torch.no_grad():
            max_actions = q.max(1)[1].view(-1)
            actions = max_actions.clone()
            for i in range(bs):
                sample = random.random()
                if sample < eps_threshold:
                    actions[i] = random.randrange(self.n_actions)
            return actions, max_actions

    def forward(self, articles, env, eps_threshold=0.0):
        bs = articles.batch_sizes.max().item()
        s_t = env.encode(articles)[0]  # [bs, d_state]
        q = []  # q-values list
        t = []  # targets list
        r = []  # rewards list
        a = []  # actions list
        for step in range(self.max_steps):
            out = self.body(s_t)  # [bs, n_actions]
            a_t, max_a_t = self.sample_action(out, eps_threshold)  # [bs], [bs]
            a.append(a_t)
            r_t, s_t = env.step(s_t, a_t)
            r.append(r_t)
            q_t = torch.gather(out, dim=1, index=a_t.unsqueeze(1))
            q.append(q_t)
            max_q_t = torch.gather(out, dim=1, index=max_a_t.unsqueeze(1))
            t.append(max_q_t)

        q = torch.cat(q, dim=1)
        t = torch.cat(t, dim=1)
        r = torch.stack(r, dim=1)
        a = torch.stack(a, dim=1)
        t[:, :-1] = self.decay_factor * t[:, 1:]
        t[:, -1] = 0
        t += r
        return q, r, a, t
