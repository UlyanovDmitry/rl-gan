import random
import torch
import torch.nn as nn


class D(nn.Module):
    def __init__(self, d_emb, d_h, vocab_size, seq_len):
        super().__init__()
        self.seq_len = seq_len
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)
        self.arnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)

        self.trnns = nn.ModuleList([nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
                                    for _ in range(seq_len)])
        self.reward_fn = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=d_h),
            nn.ReLU(),
            nn.Linear(in_features=d_h, out_features=seq_len))
        self.h_0 = nn.Parameter(torch.randn(1, 1, d_h), requires_grad=True)

    def encode(self, text):
        bs = text.batch_sizes.max().item()
        encoded = nn.utils.rnn.PackedSequence(data=self.embeddings(text.data),
                                              batch_sizes=text.batch_sizes,
                                              sorted_indices=text.sorted_indices,
                                              unsorted_indices=text.unsorted_indices)
        _, h = self.arnn(encoded, self.h_0.expand(-1, bs, -1).contiguous())
        return h

    def forward(self, articles, titles):
        h0 = self.encode(articles)
        embedded = self.embeddings(titles)

        h = []

        for i in range(self.seq_len):
            _, h_i = self.trnns[i](embedded[:, :(i+1)], h0)
            h.append(h_i.squeeze(0))

        h = torch.stack(h, dim=1)
        all_r = self.reward_fn(h)  # [bs, seq_len, seq_len]
        r = torch.diagonal(all_r, dim1=-2, dim2=-1)
        return r  # .sum(-1)

    def step(self, buf, a_t, i):
        sl, bs, d_h = buf.size()
        assert sl == self.seq_len - i
        buf = buf.unsqueeze(1)  # [seq_len-i, 1, bs, d_h]
        a_t = a_t.unsqueeze(1)

        embedded = self.embeddings(a_t)
        h = []
        for j in range(sl):
            _, h_j = self.trnns[j+i](embedded, buf[j].contiguous())
            h.append(h_j.squeeze(0))

        h = torch.stack(h, dim=0)  # [seq_len-i, bs, d_h]
        s = h[0]  # [bs, d_h]
        buf = h[1:]
        r = self.reward_fn(s)  # [bs, seq_len]
        r = r[:, i].sigmoid()
        return r, s, buf


class G(nn.Module):
    def __init__(self, d_state, n_actions, max_steps=32, decay_factor=0.99):
        super().__init__()
        self.n_actions = n_actions
        self.max_steps = max_steps
        self.decay_factor = decay_factor
        self.body = nn.Sequential(
            nn.Linear(d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, n_actions)
        )

    def sample_action(self, q, eps_threshold):
        bs = q.size(0)
        with torch.no_grad():
            max_actions = q.max(1)[1].view(-1)
            actions = max_actions.clone()
            for i in range(bs):
                sample = random.random()
                if sample < eps_threshold:
                    actions[i] = random.randrange(self.n_actions)
            return actions, max_actions

    def forward(self, articles, env, eps_threshold=0.0):
        bs = articles.batch_sizes.max().item()
        s_t = env.encode(articles)[0]  # [bs, d_state]
        buf = s_t.repeat(env.seq_len, 1).view(env.seq_len, bs, s_t.size(1))  # [seq_len, bs, d_h]

        q = []  # q-values list
        t = []  # targets list
        r = []  # rewards list
        a = []  # actions list
        for step in range(self.max_steps):
            out = self.body(s_t)  # [bs, n_actions]
            a_t, max_a_t = self.sample_action(out, eps_threshold)  # [bs], [bs]
            a.append(a_t)
            r_t, s_t, buf = env.step(buf, a_t, step)
            r.append(r_t)
            q_t = torch.gather(out, dim=1, index=a_t.unsqueeze(1))
            q.append(q_t)
            max_q_t = torch.gather(out, dim=1, index=max_a_t.unsqueeze(1))
            t.append(max_q_t)

        q = torch.cat(q, dim=1)
        t = torch.cat(t, dim=1)
        r = torch.stack(r, dim=1)
        a = torch.stack(a, dim=1)
        t[:, :-1] = self.decay_factor * t[:, 1:]
        t[:, -1] = 0
        t += r
        return q, r, a, t
