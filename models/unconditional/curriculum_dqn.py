import os, sys
import time
import numpy as np
from statistics import mean
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.distributions.gumbel import Gumbel
from torch.nn.utils.rnn import PackedSequence, pack_sequence, pad_sequence, pad_packed_sequence
import visdom
import random
import math
from datetime import datetime

from utils import language_helpers
from utils import get_accuracy, fid
from utils import ExampleStorage
from vocabulary import ReducedSymbolVocabulary
from dataset import EMNLP2017Dataset


class VariationalEncoder(nn.Module):
    def __init__(self, d_emb, d_h, d_lat, vocab_size):
        super().__init__()
        self.d_lat = d_lat
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)
        self.rnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.h_0 = nn.Parameter(torch.randn(1, 1, d_h))
        self.distr_p = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=2 * d_lat),
            nn.LeakyReLU(),
            nn.Linear(in_features=2 * d_lat, out_features=2 * d_lat))

    def forward(self, texts):
        r"""
        in:
            texts.size() == [bs, m_len]
        out:
            z.size() == [bs, d_lat]
        """
        bs = texts.batch_sizes[0]
        embedded = PackedSequence(self.embeddings(texts.data),
                                  batch_sizes=texts.batch_sizes,
                                  sorted_indices=texts.sorted_indices,
                                  unsorted_indices=texts.unsorted_indices)
        _, h = self.rnn(embedded, self.h_0.expand(-1, bs, -1).contiguous())  # fixme
        p = self.distr_p(h[0])  # [bs, 2 * d_lat]
        mu = p[:, :self.d_lat]
        sigma = F.softplus(p[:, self.d_lat:])

        z = torch.randn(bs, self.d_lat, device=texts.data.device) * sigma + mu
        return z, (mu, sigma)


class D(nn.Module):
    def __init__(self, d_emb, d_h, d_lat, vocab_size):
        super().__init__()
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)
        self.rnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.reward_fn = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=d_h),
            nn.ReLU(),
            nn.Linear(in_features=d_h, out_features=1))
        self.hw = nn.Linear(in_features=d_lat, out_features=d_h)

    def forward(self, z, texts):
        r"""
        in:
            z.size() == [bs, d_lat] -- latent representation of texts
            texts.size() == [bs, m_len]
        out:
            r.size() == [bs, m_len]
        """
        h0 = self.hw(z).view(1, z.size(0), -1)
        embedded = self.embeddings(texts)
        h, _ = self.rnn(embedded, h0)
        r = self.reward_fn(h).squeeze(-1)
        return r  # .sum(-1)

    def reset(self, z):
        return self.hw(z)

    def step(self, s_t, a_t):
        r"""
        in:
            s_t.size() == [bs, d_h]
            a_t.size() == [bs]
        out:
            r_t.size() == [bs]
            s.size() == [bs, d_h]
        """
        bs = s_t.size(0)
        s_t = s_t.unsqueeze(0)
        a_t = a_t.unsqueeze(1)

        embedded = self.embeddings(a_t)
        h, _ = self.rnn(embedded, s_t.contiguous())
        r = self.reward_fn(h).squeeze(-1)

        return r.view(-1), h.squeeze(1)


class G(nn.Module):
    def __init__(self, d_state, n_actions, d_lat):
        super().__init__()
        self.d_lat = d_lat
        self.n_actions = n_actions
        self.body = nn.Sequential(
            nn.Linear(d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, n_actions)
        )

    def sample_action(self, q, eps_threshold):
        r"""
        in:
            q.size() == [bs, n_actions]
        out:
            a.size() == [bs]
        """
        bs = q.size(0)
        with torch.no_grad():
            max_actions = q.max(1)[1].view(-1)
            actions = max_actions.clone()
            for i in range(bs):
                sample = random.random()
                if sample < eps_threshold:
                    actions[i] = random.randrange(self.n_actions)
            return actions, max_actions

    def forward(self, texts, mask, env, max_steps=32, eps_threshold=0.0):
        z = torch.randn(bs, self.d_lat).to(self.body[0].weight.device)  # fixme # [bs, d_latent]
        s_t = env.reset(z)  # [bs, d_state]
        q = []  ## q-values list
        t = []  ## targets list
        r = []  ## rewards list
        a = []  ## actions list
        for step in range(max_steps):
            out = self.body(s_t)  # [bs, n_actions]
            a_t, max_a_t = self.sample_action(out, eps_threshold)  # [bs], [bs]
            a.append(a_t)
            r_t, s_t = env.step(s_t, a_t)
            r.append(r_t)
            q_t = torch.gather(out, dim=1, index=a_t.unsqueeze(1))
            q.append(q_t)
            max_q_t = torch.gather(out, dim=1, index=max_a_t.unsqueeze(1))
            t.append(max_q_t)

        q = torch.cat(q, dim=1)
        t = torch.cat(t, dim=1)
        r = torch.stack(r, dim=1)
        a = torch.stack(a, dim=1)
        t[:, :-1] = 0.99 * t[:, 1:]
        t[:, -1] = 0
        t += r
        return q, r, a, t, z
