import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import PackedSequence


class VariationalEncoder(nn.Module):
    def __init__(self, d_emb, d_h, d_lat, vocab_size):
        super().__init__()
        self.d_lat = d_lat
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)
        self.rnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.h_0 = nn.Parameter(torch.randn(1, 1, d_h))
        self.distr_p = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=2 * d_lat),
            nn.LeakyReLU(),
            nn.Linear(in_features=2 * d_lat, out_features=2 * d_lat))

    def forward(self, texts):
        r"""
        in:
            texts.size() == [bs, m_len]
        out:
            z.size() == [bs, d_lat]
        """
        bs = texts.batch_sizes[0]
        embedded = PackedSequence(self.embeddings(texts.data.to(self.h_0.device)),
                                  batch_sizes=texts.batch_sizes,
                                  sorted_indices=texts.sorted_indices.to(self.h_0.device),
                                  unsorted_indices=texts.unsorted_indices.to(self.h_0.device))
        _, h = self.rnn(embedded, self.h_0.expand(-1, bs, -1).contiguous())  # fixme
        p = self.distr_p(h[0])  # [bs, 2 * d_lat]
        mu = p[:, :self.d_lat]
        sigma = F.softplus(p[:, self.d_lat:])

        z = torch.randn(bs, self.d_lat, device=self.h_0.device) * sigma + mu
        return z, (mu, sigma)



class D(nn.Module):
    def __init__(self, d_emb, d_h, d_lat, vocab_size):
        super().__init__()
        self.embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=d_emb)
        self.rnn = nn.GRU(input_size=d_emb, hidden_size=d_h, batch_first=True)
        self.reward_fn = nn.Sequential(
            nn.Linear(in_features=d_h, out_features=d_h),
            nn.ReLU(),
            nn.Linear(in_features=d_h, out_features=1))
        self.hw = nn.Linear(in_features=d_lat, out_features=d_h)

        self._lx = nn.Linear(in_features=d_emb, out_features=3 * d_h, bias=True)
        self._lh = nn.Linear(in_features=d_h, out_features=3 * d_h, bias=True)
        self._lx.weight.data = self.rnn.weight_ih_l0.data
        self._lx.bias.data = self.rnn.bias_ih_l0.data
        self._lh.weight.data = self.rnn.weight_hh_l0.data
        self._lh.bias.data = self.rnn.bias_hh_l0.data

    def forward(self, z, texts):
        r"""
        in:
            z.size() == [bs, d_lat] -- latent representation of texts
            texts.size() == [bs, m_len]
        out:
            r.size() == [bs, m_len]
        """
        h0 = self.hw(z).view(1, z.size(0), -1)
        embedded = self.embeddings(texts)
        h, _ = self.rnn(embedded, h0)
        r = self.reward_fn(h).squeeze(-1)
        return r  # .sum(-1)

    def reset(self, z):
        self._lx.weight.data = self.rnn.weight_ih_l0.data
        self._lx.bias.data = self.rnn.bias_ih_l0.data
        self._lh.weight.data = self.rnn.weight_hh_l0.data
        self._lh.bias.data = self.rnn.bias_hh_l0.data
        return self.hw(z)

    def step(self, s_t, a_t):
        r"""
        in:
            s_t.size() == [bs, d_h]
            a_t.size() == [bs]
        out:
            r_t.size() == [bs]
            s.size() == [bs, d_h]
        """
        bs = s_t.size(0)
        s_t = s_t.unsqueeze(0)
        a_t = a_t.unsqueeze(1)

        embedded = self.embeddings(a_t)
        h, _ = self.rnn(embedded, s_t.contiguous())
        r = self.reward_fn(h).squeeze(-1)

        return r.view(-1), h.squeeze(1)


class G(nn.Module):
    def __init__(self, d_state, d_lat, n_actions, max_steps=32, decay_factor=0.99):
        super().__init__()
        self.d_lat = d_lat
        self.n_actions = n_actions
        self.max_steps = max_steps
        self.d_state = d_state
        self.decay_factor = decay_factor
        self.body = nn.Sequential(
            nn.Linear(d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 2 * d_state),
            nn.LeakyReLU(),
            nn.Linear(2 * d_state, 1)
        )

    def forward(self, bs, env, eps_threshold=0.0):
        v = []  ## q-values list
        t = []  ## targets list
        r = []  ## rewards list
        a = []  ## actions list

        with torch.no_grad():
            z = torch.randn(bs, self.d_lat).to(self.body[0].weight.device)  # fixme # [bs, d_latent]
            s_t = env.reset(z).unsqueeze(1)  # [bs, 1, d_state]
            all_actions = env.embeddings.weight
            wx = env._lx(all_actions).unsqueeze(0).expand(bs, -1, -1)  # [bs, n_actions, 3 * d_s]

        for step in range(self.max_steps):
            with torch.no_grad():
                s_aug = s_t.expand(-1, self.n_actions, -1)  # [bs, n_actions, d_state]
                ws = env._lh(s_t)  # [bs, 1, 3 * d_state]
                ws = ws.expand(-1, self.n_actions, -1)  # [bs, n_actions, 3 * d_state]

                rx = wx[:, :, :self.d_state]
                zx = wx[:, :, self.d_state:2 * self.d_state]
                cx = wx[:, :, 2 * self.d_state:]

                rh = ws[:, :, :self.d_state]
                zh = ws[:, :, self.d_state:2 * self.d_state]
                ch = ws[:, :, 2 * self.d_state:]

                z_gate = (zx + zh).sigmoid()
                r_gate = (rx + rh).sigmoid()
                c = (cx + ch * r_gate).tanh()

                new_s = z_gate * s_aug + (1 - z_gate) * c  # [bs, n_actions, d_state]
                all_r = env.reward_fn(new_s).view(bs, -1)  # [bs, n_actions]
                all_v = self.body(new_s).view(bs, -1)  # [bs, n_actions]
                q = all_v + all_r  # [bs, n_actions]

                max_q, max_a = q.max(1)

                probs = torch.empty(bs, self.n_actions, device=q.device).fill_(
                    eps_threshold / self.n_actions)  # (self.n_actions - 1))
                p_mask = (torch.arange(self.n_actions, device=q.device).view(1, -1).repeat(bs, 1) == max_a.view(-1,
                                                                                                                1)).float()
                probs += p_mask * (1 - eps_threshold)
                a_t = torch.multinomial(probs, 1, True).view(-1)

                s_t = torch.gather(new_s, dim=1, index=a_t.view(-1, 1, 1).expand(-1, -1, self.d_state))
                r_t = torch.gather(all_r, dim=1, index=a_t.view(-1, 1)).view(-1)
                t_t = self.body(
                    torch.gather(new_s, dim=1, index=max_a.view(-1, 1, 1).expand(-1, -1, self.d_state))).view(-1)

            v_t = self.body(s_t).view(-1)

            v.append(v_t)
            r.append(r_t)
            a.append(a_t)
            t.append(t_t)

        v = torch.stack(v, dim=1)
        r = torch.stack(r, dim=1)
        a = torch.stack(a, dim=1)

        t = torch.stack(t, dim=1)

        t[:, :-1] = self.decay_factor * t[:, 1:]
        t[:, -1] = 0
        t += r

        return v, r, a, t, z
