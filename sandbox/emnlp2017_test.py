import pickle

from vocabulary import EMNLP2017voc as reader
import os
from config import DATA_PATH, SAVED_MODELS_PATH
import os, sys
import time
import numpy as np
from statistics import mean
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.distributions.gumbel import Gumbel
from torch.nn.utils.rnn import PackedSequence, pack_sequence, pad_sequence, pad_packed_sequence
import visdom
import random
import math
from datetime import datetime
from torch.utils.data import Dataset, DataLoader
from models.unconditional.dqn import D, G, VariationalEncoder
from utils import get_accuracy, fid
from utils import ExampleStorage
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def display_example(voc, vis, best_examples):
    real_text, gen_text, real_scores, gen_scores = best_examples
    g_scores = list(gen_scores)
    g_tokens = gen_text
    g_text = ''.join(voc.ids2text(g_tokens))
    g_tokens = [voc.inv_charmap[t] for t in g_tokens]

    r_scores = list(real_scores)
    r_tokens = real_text
    r_text = ''.join(voc.ids2text(r_tokens))
    r_tokens = [voc.inv_charmap[t] for t in r_tokens]

    fig = make_subplots(
        rows=2, cols=1,
        subplot_titles=(f'<b>Generated (Score {sum(g_scores):.3f}):</b> {g_text}',
                        f'<b>Real      (Score {sum(r_scores):.3f}):</b> {r_text}'))

    fig.add_trace(go.Bar(
        x=[f'({j}){t}' for j, t in enumerate(g_tokens)],
        y=g_scores,
        marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in g_scores],
        name='Generated'
    ),
        row=1, col=1)

    fig.add_trace(go.Bar(
        x=[f'({j}){t}' for j, t in enumerate(r_tokens)],
        y=r_scores,
        marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in r_scores],
        name='Real'
    ),
        row=2, col=1)

    fig.update_layout(title_text=f'<b>Examples:</b>',
                      showlegend=False)
    dict_data = fig.to_plotly_json()
    dict_data['win'] = 'train_examples'
    vis._send(dict_data)


class EMNLP2017Voc:
    def __init__(self, word_to_id):
        self.charmap = word_to_id
        self.inv_charmap = {v: k for k, v in word_to_id.items()}  # list(word_to_id.keys())

    def __len__(self):
        return len(self.charmap)

    def ids2text(self, sequence):
        words = []
        for token_index in sequence:
            if token_index in self.inv_charmap:
                words.append(self.inv_charmap[token_index])
            else:
                words.append(reader.UNK)
        return ' '.join(words).rstrip()


class EMNLP2017Dataset(Dataset):
    def __init__(self, data):
        super().__init__()
        self.sequences = data['sequences']
        self.sequence_lengths = data['sequence_lengths']

    def __len__(self):
        return len(self.sequences)

    def __getitem__(self, idx):
        return self.sequences[idx], self.sequence_lengths[idx]


def custom_collate(samples):
    seq = [s[0] for s in samples]
    seq_len = [s[1] for s in samples]
    bs = len(seq)
    tensor_seq = [torch.from_numpy(seq[i][:seq_len[i]]).long() for i in range(bs)]
    packed_texts = pack_sequence(tensor_seq, enforce_sorted=False)  # for encoder
    padded_texts = torch.from_numpy(np.stack(seq)).long()  # for discriminator
    return packed_texts, padded_texts


def save_state(e, g, d,
               e_opt, g_opt, d_opt,
               vis, env_name,
               step, epoch, path):
    torch.save(e.state_dict(), os.path.join(path, 'e.pth'))
    torch.save(d.state_dict(), os.path.join(path, 'd.pth'))
    torch.save(g.state_dict(), os.path.join(path, 'g.pth'))
    torch.save(e_opt.state_dict(), os.path.join(path, 'e_opt.pth'))
    torch.save(d_opt.state_dict(), os.path.join(path, 'd_opt.pth'))
    torch.save(g_opt.state_dict(), os.path.join(path, 'g_opt.pth'))
    vis.save([env_name])
    with open(os.path.join(path, 'steps.pickle'), 'wb') as file:
        pickle.dump((epoch, step), file)


data_dir = os.path.join(DATA_PATH, 'emnlp2017')
save_path = os.path.join(SAVED_MODELS_PATH, '2020.05.22 EMNLP2017 DVN')
dataset = "emnlp2017"

BS = 64
EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 100
N_EPOCHES = 100
STEPS_PER_DISPLAY = 20
ENV_NAME = 'EMNLP2017_DVN+variational encoder (22.05.2020)'

if __name__ == '__main__':
    raw_data = reader.get_raw_data(
        data_path=data_dir, dataset=dataset)
    train_data, valid_data, word_to_id = raw_data

    train_dataset = EMNLP2017Dataset(train_data)
    valid_dataset = EMNLP2017Dataset(valid_data)
    voc = EMNLP2017Voc(word_to_id)
    vis = visdom.Visdom(env=ENV_NAME)

    e = VariationalEncoder(256, 512, 64, len(voc)).cuda()
    d = D(256, 512, 64, len(voc)).cuda()
    g = G(512, len(voc), 64).cuda()

    e_opt = optim.Adam(e.parameters())
    d_opt = optim.Adam(d.parameters(), lr=1e-4)
    g_opt = optim.RMSprop(g.parameters(), lr=1e-3)

    epoch = 0
    step = 0

    train_dataloader = DataLoader(train_dataset,
                                  batch_size=BS,
                                  shuffle=True,
                                  collate_fn=custom_collate)
    e_losses = []
    d_losses = []
    g_losses = []
    accuraces = []
    d_accuraces = []
    real_rewards = []
    fake_rewards = []
    best_example = [None, None, None, None, None, None]

    storage = ExampleStorage(voc, 1)

    d_loss_fn = nn.BCEWithLogitsLoss()
    g_loss_fn = nn.SmoothL1Loss()

    print('Start training')

    for epoch in range(N_EPOCHES):
        for (packed_texts, padded_texts) in train_dataloader:

            if step % 1000 == 0:
                save_state(e, g, d,
                           e_opt, g_opt, d_opt,
                           vis, ENV_NAME, step, epoch, save_path)


            packed_texts = packed_texts.cuda()
            padded_texts = padded_texts.cuda()

            eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * step / EPS_DECAY)

            act_bs = padded_texts.size(0)
            ## d train step
            e_opt.zero_grad()
            d_opt.zero_grad()

            _, _, fake_texts, _, z_fake = g(act_bs, d,
                                            max_steps=reader.MAX_TOKENS_SEQUENCE[dataset],
                                            eps_threshold=0)
            scores_fake = d(z_fake, fake_texts)
            r_fake = scores_fake.sum(-1)

            z_real, (mu, sigma) = e(packed_texts)
            scores_real = d(z_real, padded_texts)  # torch.randn(act_bs, d_lat).cuda(), padded_texts)
            r_real = scores_real.sum(-1)

            d_loss = (d_loss_fn(r_real, torch.ones_like(r_real)) + d_loss_fn(r_fake, torch.zeros_like(r_fake))) / 2
            e_kld = 10 * -0.5 * (1 + 2 * sigma.log() - mu.pow(2) - sigma.pow(2)).sum()

            d_loss.backward(retain_graph=True)
            d_opt.step()

            e_kld.backward()
            e_opt.step()

            e_losses.append(e_kld.cpu().detach().item())
            d_losses.append(d_loss.cpu().detach().item())

            t = torch.cat([r_real.clone().fill_(1.0),
                           r_fake.clone().fill_(0.0)], dim=0).view(-1).cpu().detach().tolist()
            p = torch.cat([r_real.sigmoid(),
                           r_fake.sigmoid()], dim=0).view(-1).cpu().detach().tolist()

            acc, d_acc = get_accuracy(t, p, 0.5)
            accuraces.append(acc)
            d_accuraces.append(d_acc)

            real_rewards += r_real.cpu().detach().tolist()
            fake_rewards += r_fake.cpu().detach().tolist()

            ## g train step
            g_opt.zero_grad()
            q, r, a, t, z = g(act_bs, d,
                              max_steps=reader.MAX_TOKENS_SEQUENCE[dataset],
                              eps_threshold=eps_threshold)
            g_loss = g_loss_fn(q, t.detach())
            g_loss.backward()
            torch.nn.utils.clip_grad_norm_(g.parameters(), 1.0)
            g_opt.step()
            g_losses.append(g_loss.cpu().detach().item())

            # get best example
            with torch.no_grad():
                i_max = r_fake.argmax()
                if best_example[0] is None or best_example[0] < r_fake[i_max]:
                    best_example[0] = r_fake[i_max].cpu()
                    best_example[2] = fake_texts[i_max].cpu().tolist()
                    best_example[4] = scores_fake[i_max].cpu()
                i_max = r_real.argmax()
                if best_example[1] is None or best_example[1] < r_real[i_max]:
                    best_example[1] = r_real[i_max].cpu()
                    best_example[3] = padded_texts[i_max].cpu().tolist()
                    best_example[5] = scores_real[i_max].cpu()

            if step % STEPS_PER_DISPLAY == 0:
                vis.line(X=[step], Y=[mean(e_losses)], update='append', name='E KLD', win='gan_losses')
                vis.line(X=[step], Y=[mean(d_losses)], update='append', name='D loss', win='gan_losses')
                vis.line(X=[step], Y=[mean(g_losses)], update='append', name='G loss', win='gan_losses')
                e_losses = []
                d_losses = []
                g_losses = []

                vis.line(X=[step], Y=[mean(accuraces)], update='append', name='Acc', win='gan_acc')
                vis.line(X=[step], Y=[mean(d_accuraces)], update='append', name='Def acc', win='gan_acc')
                accuraces = []
                d_accuraces = []

                vis.line(X=[step], Y=[mean(real_rewards)], update='append', name='Real r', win='rewards')
                vis.line(X=[step], Y=[mean(fake_rewards)], update='append', name='Fake r', win='rewards')
                real_rewards = []
                fake_rewards = []

                display_example(voc, vis, best_example[2:])
                best_example = [None, None, None, None, None, None]

            step += 1
