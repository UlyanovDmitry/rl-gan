from dataset import GigawordDataset
from vocabulary import ReducedSymbolVocabulary
from models.summarization.curriculum_dqn import G, D
from trainers import SummarizationTrainer
import torch.optim as optim
import config
import torch
import os
import pickle
import pandas as pd
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import PackedSequence, pack_sequence, pad_sequence, pad_packed_sequence
from utils import fid
import pickle
from rouge_score import rouge_scorer
from rouge_score import scoring
import config
import json
import tqdm

if __name__ == '__main__':
    BS = 64
    N_STEPS = 200001
    MAX_LEN = 32
    STEPS_PER_DISPLAY = 50

    voc = ReducedSymbolVocabulary()

    D_S = 512
    D_EMB = 32
    D_ACT = len(voc)

    EPS_START = 0.9
    EPS_END = 0.01
    EPS_DECAY = 100

    d = D(D_EMB, D_S, D_ACT).cuda().eval()
    g = G(D_S, D_ACT, MAX_LEN).cuda().eval()

    env_name = 'Summarization Gigaword Curriculum DQN (2020.05.23) 512-32, max_len=32, symbol lvl'
    if not os.path.exists(os.path.join(config.SAVED_TEST_RESULTS_PATH, env_name)):
        os.mkdir(os.path.join(config.SAVED_TEST_RESULTS_PATH, env_name))

    d.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'best_fed', 'd.pth')))
    g.load_state_dict(torch.load(os.path.join(config.SAVED_MODELS_PATH, env_name, 'best_fed', 'g.pth')))

    test_dataset = GigawordDataset(voc, voc, 'test', max_t_len=MAX_LEN, path='../data/gigaword/')

    def custom_collate_fn(samples):
        articles = [torch.from_numpy(s[0]).long() for s in samples]
        titles = pad_sequence([torch.from_numpy(s[1]).long() for s in samples],
                              batch_first=True, padding_value=voc.PAD_IDX)
        return articles, titles


    scorer = rouge_scorer.RougeScorer(['rouge1', 'rouge2', 'rougeL'], use_stemmer=True)
    aggregator = scoring.BootstrapAggregator()
    test_dataloader = DataLoader(test_dataset, batch_size=BS, shuffle=False,
                                 collate_fn=custom_collate_fn)

    articles = []
    titles = []
    generated_titles = []

    with torch.no_grad():
        v_mask = torch.ones(BS, MAX_LEN).int().cuda()

        with torch.no_grad():
            for (art, ttl) in tqdm.tqdm(test_dataloader):
                act_bs = ttl.size(0)
                ttl = ttl.cuda()
                _, _, gen, _ = g(pack_sequence(art, enforce_sorted=False).cuda(), ttl.clone().fill_(0),
                                 v_mask[:act_bs], d, eps_threshold=0.0)  # may be 0
                for i in range(act_bs):
                    art_text = ''.join(voc.ids2text(art[i], return_special=False))
                    ttl_text = ''.join(voc.ids2text(ttl[i], return_special=False))
                    gen_text = ''.join(voc.ids2text(gen[i], return_special=False))
                    aggregator.add_scores(scorer.score(gen_text, ttl_text))

                    articles.append(art_text)
                    titles.append(ttl_text)
                    generated_titles.append(gen_text)

    rouge_score = aggregator.aggregate()
    fid_score = fid(generated_titles, titles)

    examples = pd.DataFrame(data={'articles': articles,
                                  'titles': titles,
                                  'generated': generated_titles})
    examples.to_csv(os.path.join(config.SAVED_TEST_RESULTS_PATH, env_name, 'examples.csv'))

    scores = dict(rouge_score)
    scores.update({'fed': fid_score})
    # with open(os.path.join(config.SAVED_TEST_RESULTS_PATH, env_name, 'scores.json'), "wb") as write_file:
    #     json.dump(scores, write_file)
    print(scores)
    print(scores['rouge1'].mid.recall * 100)
    print(scores['rouge2'].mid.recall * 100)
    print(scores['rougeL'].mid.fmeasure * 100)
    print(scores['fed'])
