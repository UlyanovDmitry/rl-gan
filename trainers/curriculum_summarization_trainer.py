import os
import math
from statistics import mean
import pandas as pd
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import PackedSequence, pack_sequence, pad_sequence, pad_packed_sequence
from utils import fid, get_accuracy
from utils import ExampleStorage
import visdom
from datetime import datetime
import pickle
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from rouge_score import rouge_scorer
from rouge_score import scoring
import config


class CurriculumSummarizationTrainer:
    r"""
    Trainer for DQN/DVN models
    """

    def __init__(self, env_name, voc, train_dataset, valid_dataset):
        self.env_name = env_name  # f'({datetime.now().strftime("%Y.%m.%d")}) ' + env_name
        self.save_path = os.path.join(config.SAVED_MODELS_PATH, self.env_name)
        self.vis = visdom.Visdom(env=self.env_name)
        self.voc = voc
        self.train_dataset = train_dataset
        self.valid_dataset = valid_dataset

        if not os.path.exists(self.save_path):
            os.mkdir(self.save_path)

    def save_state(self, g, d,
                   g_opt, d_opt,
                   step, epoch, subpath):
        torch.save(d.state_dict(), os.path.join(self.save_path, subpath, 'd.pth'))
        torch.save(g.state_dict(), os.path.join(self.save_path, subpath, 'g.pth'))
        torch.save(d_opt.state_dict(), os.path.join(self.save_path, subpath, 'd_opt.pth'))
        torch.save(g_opt.state_dict(), os.path.join(self.save_path, subpath, 'g_opt.pth'))
        self.vis.save([self.env_name])
        with open(os.path.join(self.save_path, 'steps.pickle'), 'wb') as file:
            pickle.dump((epoch, step), file)

    def display_example(self, article, real_title, gen_title, real_scores, gen_scores):
        g_scores = list(gen_scores)
        g_tokens = gen_title
        g_text = ''.join(self.voc.ids2text(g_tokens))
        g_tokens = [self.voc.inv_charmap[t] for t in g_tokens]

        r_scores = list(real_scores)
        r_tokens = real_title
        r_text = ''.join(self.voc.ids2text(r_tokens))
        r_tokens = [self.voc.inv_charmap[t] for t in r_tokens]

        article_text = ''.join(self.voc.ids2text(article))

        fig = make_subplots(
            rows=2, cols=1,
            subplot_titles=(f'<b>Generated (Score {sum(g_scores):.3f}):</b> {g_text}',
                            f'<b>Real      (Score {sum(r_scores):.3f}):</b> {r_text}'))

        fig.add_trace(go.Bar(
            x=[f'({j}){t}' for j, t in enumerate(g_tokens)],
            y=g_scores,
            marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in g_scores],
            name='Generated'
        ),
            row=1, col=1)

        fig.add_trace(go.Bar(
            x=[f'({j}){t}' for j, t in enumerate(r_tokens)],
            y=r_scores,
            marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in r_scores],
            name='Real'
        ),
            row=2, col=1)

        fig.update_layout(title_text=f'<b>Article:</b> {article_text}',
                          showlegend=False)
        dict_data = fig.to_plotly_json()
        dict_data['win'] = 'train_examples'
        self.vis._send(dict_data)

    def custom_collate_fn(self, samples):
        articles = [torch.from_numpy(s[0]).long() for s in samples]
        titles = pad_sequence([torch.from_numpy(s[1]).long() for s in samples],
                              batch_first=True, padding_value=self.voc.PAD_IDX)
        return articles, titles

    def train(self, g, d,
              g_opt, d_opt,
              bs,
              step=0,
              epoch=0,
              best_scores=None,
              n_epoches=100,
              steps_per_display=50,
              steps_per_save=1000,
              steps_per_validation=5000,
              eps_end=0.05, eps_start=0.9, eps_decay=100, max_step=200000):
        d_losses = []
        g_losses = []
        accuraces = []
        d_accuraces = []
        real_rewards = []
        fake_rewards = []
        best_example = (None, None, None, None)

        # mask init
        g_mask = torch.ones(bs, self.train_dataset.max_t_len).cuda()
        for i in range(bs):
            g_mask[i, :int(i / bs * self.train_dataset.max_t_len)] = 0

        g_mask_i = g_mask.int()

        d_mask = torch.ones(bs, self.train_dataset.max_t_len).cuda()  # g_mask
        d_mask_i = d_mask.int()
        dropout_w = self.train_dataset.max_t_len / d_mask.sum(-1)

        d_loss_fn = nn.BCEWithLogitsLoss()
        g_loss_fn = nn.SmoothL1Loss()

        train_dataloader = DataLoader(self.train_dataset, batch_size=3 * bs, shuffle=True,
                                      collate_fn=self.custom_collate_fn)

        for c_epoch in range(epoch, n_epoches):
            for (articles, titles) in train_dataloader:
                if step >= max_step:
                    self.save_state(g, d, g_opt, d_opt, step, epoch, 'current')
                    return
                if step % steps_per_validation == 0:
                    print(f'Start validation, step: {step}')
                    rouge_score, fid_score, examples = self.validation(g, d, bs, limit=1000)
                    self.vis.line([rouge_score['rouge1'].mid.fmeasure], [step],
                                  update='append', name='rouge1', win='rouge_scores')
                    self.vis.line([rouge_score['rouge2'].mid.fmeasure], [step],
                                  update='append', name='rouge2', win='rouge_scores')
                    self.vis.line([rouge_score['rougeL'].mid.fmeasure], [step],
                                  update='append', name='rougeL', win='rouge_scores')
                    self.vis.line([fid_score], [step], update='append', name='fid', win='fid')

                    if best_scores['rouge1'] is None or best_scores['rouge1'] < rouge_score['rouge1'].mid.fmeasure:
                        best_scores['rouge1'] = rouge_score['rouge1'].mid.fmeasure
                        self.save_state(g, d, g_opt, d_opt, step, epoch, 'best_rouge1')
                        examples.to_csv(os.path.join(self.save_path, 'best_rouge1', 'examples.csv'))
                    if best_scores['rouge2'] is None or best_scores['rouge2'] < rouge_score['rouge2'].mid.fmeasure:
                        best_scores['rouge2'] = rouge_score['rouge2'].mid.fmeasure
                        self.save_state(g, d, g_opt, d_opt, step, epoch, 'best_rouge2')
                        examples.to_csv(os.path.join(self.save_path, 'best_rouge2', 'examples.csv'))
                    if best_scores['rougeL'] is None or best_scores['rougeL'] < rouge_score['rougeL'].mid.fmeasure:
                        best_scores['rougeL'] = rouge_score['rougeL'].mid.fmeasure
                        self.save_state(g, d, g_opt, d_opt, step, epoch, 'best_rougeL')
                        examples.to_csv(os.path.join(self.save_path, 'best_rougeL', 'examples.csv'))
                    if best_scores['fed'] is None or best_scores['fed'] > fid_score:
                        best_scores['fed'] = fid_score
                        self.save_state(g, d, g_opt, d_opt, step, epoch, 'best_fed')
                        examples.to_csv(os.path.join(self.save_path, 'best_fed', 'examples.csv'))

                    with open(os.path.join(self.save_path, 'best_scores.pickle'), 'wb') as file:
                        pickle.dump(best_scores, file)

                if step % steps_per_save == 0:
                    self.save_state(g, d, g_opt, d_opt, step, epoch, 'current')

                full_bs = titles.size(0)
                if full_bs % 3 != 0:
                    continue  # the rnd of the dataset
                act_bs = full_bs // 3

                dra = pack_sequence(articles[:act_bs], enforce_sorted=False).cuda()
                drt = titles[:act_bs].cuda()
                dfa = articles[act_bs: 2 * act_bs] #, enforce_sorted=False).cuda()
                dft = titles[act_bs: 2 * act_bs].cuda()
                gra = articles[2 * act_bs:]
                grt = titles[2 * act_bs:].cuda()

                eps_threshold = eps_end + (eps_start - eps_end) * math.exp(-1. * step / eps_decay)

                # d train step
                with torch.no_grad():
                    _, _, fake_titles, _ = g(pack_sequence(dfa, enforce_sorted=False).cuda(), dft,
                                             d_mask_i[:dft.size(0)], d, eps_threshold=0.0)
                d_opt.zero_grad()
                scores_real = d(dra, drt)
                r_real = scores_real.sum(-1)
                scores_fake = d(pack_sequence(dfa, enforce_sorted=False).cuda(), fake_titles)
                r_fake = (scores_fake * d_mask).sum(-1) * dropout_w
                d_loss = (d_loss_fn(r_real, torch.ones_like(r_real)) + d_loss_fn(r_fake, torch.zeros_like(r_fake))) / 2
                d_loss.backward()
                d_opt.step()
                d_losses.append(d_loss.cpu().detach().item())
                t = torch.cat([r_real.clone().fill_(1.0),
                               r_fake.clone().fill_(0.0)], dim=0).view(-1).cpu().detach().tolist()
                p = torch.cat([r_real.sigmoid(),
                               r_fake.sigmoid()], dim=0).view(-1).cpu().detach().tolist()

                acc, d_acc = get_accuracy(t, p, 0.5)
                accuraces.append(acc)
                d_accuraces.append(d_acc)

                real_rewards += r_real.cpu().detach().tolist()
                fake_rewards += r_fake.cpu().detach().tolist()

                # g train step
                g_opt.zero_grad()
                q, r, a, t = g(pack_sequence(gra, enforce_sorted=False).cuda(), grt,
                               g_mask_i[:grt.size(0)], d, eps_threshold=eps_threshold)
                g_loss = g_loss_fn(q, t.detach())
                g_loss.backward()
                torch.nn.utils.clip_grad_norm_(g.parameters(), 1.0)
                g_opt.step()
                g_losses.append(g_loss.cpu().detach().item())

                # get best example
                with torch.no_grad():
                    i_max = r_fake.argmax()
                    if best_example[0] is None or best_example[0] < r_fake[i_max]:
                        best_example = (r_fake[i_max].cpu(), dfa[i_max].cpu(),
                                        dft[i_max].cpu(), fake_titles[i_max].cpu(),
                                        d(pack_sequence(dfa[i_max:i_max + 1], enforce_sorted=False).cuda(),
                                          dft[i_max:i_max + 1])[0].cpu(),
                                        scores_fake[i_max].cpu())

                if step % steps_per_display == 0:
                    self.vis.line(X=[step], Y=[mean(d_losses)], update='append', name='D loss', win='gan_losses')
                    self.vis.line(X=[step], Y=[mean(g_losses)], update='append', name='G loss', win='gan_losses')
                    d_losses = []
                    g_losses = []
                    self.vis.line(X=[step], Y=[mean(accuraces)], update='append', name='Acc', win='gan_acc')
                    self.vis.line(X=[step], Y=[mean(d_accuraces)], update='append', name='Def acc', win='gan_acc')
                    accuraces = []

                    d_accuraces = []

                    self.vis.line(X=[step], Y=[mean(real_rewards)], update='append', name='Real r', win='rewards')
                    self.vis.line(X=[step], Y=[mean(fake_rewards)], update='append', name='Fake r', win='rewards')
                    real_rewards = []
                    fake_rewards = []

                    self.display_example(*best_example[1:])
                    best_example = (None, None, None, None)

                step += 1

    def validation(self, g, d, bs, eps=0.0, limit=1000000):
        scorer = rouge_scorer.RougeScorer(['rouge1', 'rouge2', 'rougeL'], use_stemmer=True)
        aggregator = scoring.BootstrapAggregator()
        valid_dataloader = DataLoader(self.valid_dataset, batch_size=bs, shuffle=False,
                                      collate_fn=self.custom_collate_fn)

        articles = []
        titles = []
        generated_titles = []

        v_mask = torch.ones(bs, self.train_dataset.max_t_len).int().cuda()

        with torch.no_grad():
            for (art, ttl) in valid_dataloader:
                act_bs = ttl.size(0)
                ttl = ttl.cuda()
                _, _, gen, _ = g(pack_sequence(art, enforce_sorted=False).cuda(), ttl,
                                 v_mask[:act_bs], d, eps_threshold=eps)  # may be 0
                for i in range(act_bs):
                    art_text = ''.join(self.voc.ids2text(art[i], return_special=False))
                    ttl_text = ''.join(self.voc.ids2text(ttl[i], return_special=False))
                    gen_text = ''.join(self.voc.ids2text(gen[i], return_special=False))
                    aggregator.add_scores(scorer.score(gen_text, ttl_text))

                    articles.append(art_text)
                    titles.append(ttl_text)
                    generated_titles.append(gen_text)
        rouge_score = aggregator.aggregate()
        fid_score = fid(generated_titles, titles)
        return rouge_score, fid_score, pd.DataFrame(data={'articles': articles,
                                                          'titles': titles,
                                                          'generated': generated_titles})
