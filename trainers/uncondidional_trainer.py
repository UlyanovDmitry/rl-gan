import os
import math
from statistics import mean
import pandas as pd
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import pack_sequence, pad_sequence, pad_packed_sequence
from utils import fid, get_accuracy
import visdom
import pickle
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from rouge_score import scoring
import config
import numpy as np


class UnconditionalTrainer:
    r"""
    Trainer for DQN/DVN models
    """
    def __init__(self, env_name, voc, train_dataset, valid_dataset):
        self.env_name = env_name  # f'({datetime.now().strftime("%Y.%m.%d")}) ' + env_name
        self.save_path = os.path.join(config.SAVED_MODELS_PATH, self.env_name)
        self.vis = visdom.Visdom(env=self.env_name)
        self.voc = voc
        self.max_len = train_dataset.max_len
        self.train_dataset = train_dataset
        self.valid_dataset = valid_dataset

    def save_state(self, g, d, e,
                   g_opt, d_opt, e_opt,
                   step, epoch, subpath):
        torch.save(d.state_dict(), os.path.join(self.save_path, subpath, 'd.pth'))
        torch.save(g.state_dict(), os.path.join(self.save_path, subpath, 'g.pth'))
        torch.save(e.state_dict(), os.path.join(self.save_path, subpath, 'e.pth'))
        torch.save(d_opt.state_dict(), os.path.join(self.save_path, subpath, 'd_opt.pth'))
        torch.save(g_opt.state_dict(), os.path.join(self.save_path, subpath, 'g_opt.pth'))
        torch.save(e_opt.state_dict(), os.path.join(self.save_path, subpath, 'e_opt.pth'))
        self.vis.save([self.env_name])
        with open(os.path.join(self.save_path, 'steps.pickle'), 'wb') as file:
            pickle.dump((epoch, step), file)

    def display_example(self, real_text, gen_text, real_scores, gen_scores):
        g_scores = list(gen_scores)
        g_tokens = gen_text
        g_text = ''.join(self.voc.ids2text(g_tokens, return_special=False))
        g_tokens = [self.voc.inv_charmap[t] for t in g_tokens]

        r_scores = list(real_scores)
        r_tokens = real_text
        r_text = ''.join(self.voc.ids2text(r_tokens, return_special=False))
        r_tokens = [self.voc.inv_charmap[t] for t in r_tokens]

        fig = make_subplots(
            rows=2, cols=1,
            subplot_titles=(f'<b>Generated (Score {sum(g_scores):.3f}):</b> {g_text}',
                            f'<b>Real      (Score {sum(r_scores):.3f}):</b> {r_text}'))

        fig.add_trace(go.Bar(
            x=[f'({j}){t}' for j, t in enumerate(g_tokens)],
            y=g_scores,
            marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in g_scores],
            name='Generated'
        ),
            row=1, col=1)

        fig.add_trace(go.Bar(
            x=[f'({j}){t}' for j, t in enumerate(r_tokens)],
            y=r_scores,
            marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in r_scores],
            name='Real'
        ),
            row=2, col=1)

        fig.update_layout(title_text=f'<b>Examples:</b>',
                          showlegend=False)
        dict_data = fig.to_plotly_json()
        dict_data['win'] = 'train_examples'
        self.vis._send(dict_data)

    def custom_collate_fn(self, samples):
        seq = [s[0] for s in samples]
        raw_texts = [s[1] for s in samples]
        bs = len(seq)
        tensor_seq = [torch.from_numpy(seq[i]).long() for i in range(bs)]
        packed_texts = pack_sequence(tensor_seq, enforce_sorted=False)  # for encoder
        padded_texts = torch.full(size=(bs, self.max_len), fill_value=self.voc.PAD_IDX, dtype=torch.long)# for discriminator
        for i in range(bs):
            padded_texts[i, :len(seq[i])] = tensor_seq[i]
        return packed_texts, padded_texts, raw_texts

    def train(self, g, d, e,
              g_opt, d_opt, e_opt,
              bs,
              step=0,
              epoch=0,
              best_scores=None,
              n_epoches=100,
              steps_per_display=50,
              steps_per_save=1000,
              steps_per_validation=5000,
              eps_end=0.05, eps_start=0.9, eps_decay=100, max_step=200000):
        e_losses = []
        d_losses = []
        g_losses = []
        accuraces = []
        d_accuraces = []
        real_rewards = []
        fake_rewards = []
        best_example = (None, None, None, None, None)

        d_loss_fn = nn.BCEWithLogitsLoss()
        g_loss_fn = nn.SmoothL1Loss()

        train_dataloader = DataLoader(self.train_dataset, batch_size=bs, shuffle=True,
                                      collate_fn=self.custom_collate_fn)

        for c_epoch in range(epoch, n_epoches):
            for (packed_texts, padded_texts, samples) in train_dataloader:
                if step >= max_step:
                    self.save_state(g, d, e, g_opt, d_opt, e_opt, step, epoch, 'current')
                    return

                if step % steps_per_validation == 0:
                    print(f'Start validation, step: {step}')
                    fid_score, examples = self.validation(g, d, e, bs)
                    self.vis.line([fid_score], [step], update='append', name='fid', win='fid')

                    if best_scores['fed'] is None or best_scores['fed'] > fid_score:
                        best_scores['fed'] = fid_score
                        self.save_state(g, d, e, g_opt, d_opt, e_opt, step, epoch, 'best_fed')
                        examples.to_csv(os.path.join(self.save_path, 'best_fed', 'examples.csv'))

                    with open(os.path.join(self.save_path, 'best_scores.pickle'), 'wb') as file:
                        pickle.dump(best_scores, file)

                    examples.to_csv(os.path.join(self.save_path, 'last_examples.csv'))

                if step % steps_per_save == 0:
                    self.save_state(g, d, e, g_opt, d_opt, e_opt, step, epoch, 'current')

                padded_texts = padded_texts.cuda()
                eps_threshold = eps_end + (eps_start - eps_end) * math.exp(-1. * step / eps_decay)
                act_bs = padded_texts.size(0)

                ##  e/d train step
                e_opt.zero_grad()
                d_opt.zero_grad()

                with torch.no_grad():
                    _, _, fake_texts, _, z_fake = g(act_bs, d, eps_threshold=0.0)  # may be 0
                scores_fake = d(z_fake, fake_texts)
                r_fake = scores_fake.sum(-1)

                z_real, (mu, sigma) = e(packed_texts)
                scores_real = d(z_real, padded_texts)
                r_real = scores_real.sum(-1)
                d_loss = (d_loss_fn(r_real, torch.ones_like(r_real)) + d_loss_fn(r_fake, torch.zeros_like(r_fake))) / 2

                d_loss.backward(retain_graph=True)
                d_opt.step()

                torch.nn.utils.clip_grad_norm_(e.parameters(), 0.1)

                e_kld = 5 * -0.5 * (1 + 2 * sigma.log() - mu.pow(2) - sigma.pow(2)).sum()

                e_kld.backward()
                torch.nn.utils.clip_grad_norm_(e.parameters(), 1.0)
                e_opt.step()

                e_losses.append(e_kld.cpu().detach().item())
                d_losses.append(d_loss.cpu().detach().item())
                t = torch.cat([r_real.clone().fill_(1.0),
                               r_fake.clone().fill_(0.0)], dim=0).view(-1).cpu().detach().tolist()
                p = torch.cat([r_real.sigmoid(),
                               r_fake.sigmoid()], dim=0).view(-1).cpu().detach().tolist()

                acc, d_acc = get_accuracy(t, p, 0.5)
                accuraces.append(acc)
                d_accuraces.append(d_acc)

                real_rewards += r_real.cpu().detach().tolist()
                fake_rewards += r_fake.cpu().detach().tolist()

                # g train step
                g_opt.zero_grad()
                q, r, a, t, _ = g(act_bs, d, eps_threshold=eps_threshold)
                g_loss = g_loss_fn(q, t.detach())
                g_loss.backward()
                torch.nn.utils.clip_grad_norm_(g.parameters(), 1.0)
                g_opt.step()
                g_losses.append(g_loss.cpu().detach().item())

                # get best example
                with torch.no_grad():
                    i_max = r_fake.argmax()
                    if best_example[0] is None or best_example[0] < r_fake[i_max]:
                        best_example = (r_fake[i_max].cpu(),
                                        padded_texts[0].cpu(),
                                        fake_texts[i_max].cpu(),
                                        scores_real[0].cpu(),
                                        scores_fake[i_max].cpu())

                if step % steps_per_display == 0:
                    self.vis.line(X=[step], Y=[mean(d_losses)], update='append', name='D loss', win='gan_losses')
                    self.vis.line(X=[step], Y=[mean(g_losses)], update='append', name='G loss', win='gan_losses')
                    self.vis.line(X=[step], Y=[mean(e_losses)], update='append', name='E loss', win='gan_losses')
                    d_losses = []
                    g_losses = []
                    e_losses = []

                    self.vis.line(X=[step], Y=[mean(accuraces)], update='append', name='Acc', win='gan_acc')
                    self.vis.line(X=[step], Y=[mean(d_accuraces)], update='append', name='Def acc', win='gan_acc')
                    accuraces = []
                    d_accuraces = []

                    self.vis.line(X=[step], Y=[mean(real_rewards)], update='append', name='Real r', win='rewards')
                    self.vis.line(X=[step], Y=[mean(fake_rewards)], update='append', name='Fake r', win='rewards')
                    real_rewards = []
                    fake_rewards = []

                    self.display_example(*best_example[1:])
                    best_example = (None, None, None, None)

                step += 1

    def validation(self, g, d, e, bs, eps=0.0, limit=1000000):
        valid_dataloader = DataLoader(self.valid_dataset, batch_size=bs, shuffle=False,
                                      collate_fn=self.custom_collate_fn)
        real_texts = []
        generated_texts = []

        with torch.no_grad():
            for (packed_texts, padded_texts, samples) in valid_dataloader:
                real_texts += samples

                act_bs = padded_texts.size(0)
                _, _, gen, _, _ = g(act_bs, d, eps_threshold=0.0)
                for i in range(act_bs):
                    gen_text = ''.join(self.voc.ids2text(gen[i], return_special=False))
                    generated_texts.append(gen_text)

        fid_score = fid(generated_texts, real_texts)
        return fid_score, pd.DataFrame(data={'examples': generated_texts})
