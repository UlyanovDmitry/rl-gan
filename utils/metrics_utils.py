from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import tensorflow.compat.v1 as tf
import tensorflow_gan as tfgan
import tensorflow_hub as hub


def fid(generated_sentences, real_sentences):
    """Compute FID rn sentences using pretrained universal sentence encoder.
    Args:
    generated_sentences: list of N strings.
    real_sentences: list of N strings.
    Returns:
    Frechet distance between activations.
    """
    embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder/2")
    real_embed = embed(real_sentences)
    generated_embed = embed(generated_sentences)
    distance = tfgan.eval.frechet_classifier_distance_from_activations(
        real_embed, generated_embed)

    # Restrict the thread pool size to prevent excessive CPU usage.
    config = tf.ConfigProto()
    config.intra_op_parallelism_threads = 16
    config.inter_op_parallelism_threads = 16
    with tf.Session(config=config) as session:
        session.run(tf.global_variables_initializer())
        session.run(tf.tables_initializer())
        distance_np = session.run(distance)
    return distance_np


def get_accuracy(target, prediction, def_value=0.5):
    assert len(target) == len(prediction)

    full_array = [v for v in zip(prediction, target)]
    full_array = sorted(full_array, key=lambda x: x[0])

    target = [v[1] for v in full_array]
    prediction = [v[0] for v in full_array]

    best_accuracy = sum(target) / len(target)
    default_accuracy = None

    for i in range(len(prediction)):
        accuracy = ((len(target[:i]) - sum(target[:i])) + sum(target[i:])) / len(target)
        if prediction[i] > def_value and default_accuracy is None:
            default_accuracy = accuracy
        best_accuracy = max(best_accuracy, accuracy)

    accuracy = (len(target) - sum(target)) / len(target)
    best_accuracy = max(best_accuracy, accuracy)

    if default_accuracy is None:
        default_accuracy = accuracy

    return best_accuracy, default_accuracy