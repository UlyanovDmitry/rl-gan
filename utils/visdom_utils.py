import visdom
import plotly.graph_objects as go
from plotly.subplots import make_subplots

class ExampleStorage:
    def __init__(self, voc, limit=10):
        self.voc = voc
        self.limit = limit
        self.generated_examples = []
        self.real_examples = []
        
    def add_gen(self, scores, tokens):
        self.generated_examples = sorted(self.generated_examples + 
                                         [(a, b) for (a, b) in zip(scores, tokens)], 
                                         key=lambda x: sum(x[0]))[:self.limit]
        
    def add_real(self, scores, tokens):
        self.real_examples = sorted(self.real_examples + 
                                    [(a, b) for (a, b) in zip(scores, tokens)], 
                                    key=lambda x: sum(x[0]))[:self.limit]
        
    def clear(self):
        self.generated_examples = []
        self.real_examples = []
        
    def show_example(self, vis: visdom.Visdom, i: int, win_name='train_examples'):
        g_scores = list(self.generated_examples[i][0])
        g_tokens = self.generated_examples[i][1]
        g_text = ''.join(self.voc.ids2text(g_tokens))
        g_tokens = [self.voc.inv_charmap[t] for t in g_tokens]
        
        r_scores = list(self.real_examples[i][0])
        r_tokens = self.real_examples[i][1]
        r_text = ''.join(self.voc.ids2text(r_tokens))
        r_tokens = [self.voc.inv_charmap[t] for t in r_tokens]
        
        fig = make_subplots(
            rows=2, cols=1,
            subplot_titles=(f'{g_text} \nScore: {sum(g_scores):.3f}', 
                            f'{r_text} \nScore: {sum(r_scores):.3f}'))

        fig.add_trace(go.Bar(
                            x=[f'({j}){t}' for j, t in enumerate(g_tokens)],
                            y=g_scores,
                            marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in g_scores],
                            name='Generated'
                        ),
                      row=1, col=1)

        fig.add_trace(go.Bar(
                            x=[f'({j}){t}' for j, t in enumerate(r_tokens)],
                            y=r_scores,
                            marker_color=[('crimson' if s >= 0 else 'steelBlue') for s in r_scores],
                            name='Real'
                        ),
                      row=2, col=1)

        fig.update_layout(title_text=f'Examples {i}',
                          showlegend=False)
        dict_data = fig.to_plotly_json()
        dict_data['win'] = win_name
        vis._send(dict_data)