import string

class SymbolVocabulary:
    UNK_IDX = 0
    UNK_TOKEN = '[UNK]'

    PAD_IDX = 1
    PAD_TOKEN = '[PAD]'

    EOS_IDX = 2
    EOS_TOKEN = '[EOS]'
    
    def __init__(self):
        self.inv_charmap = [SymbolVocabulary.UNK_TOKEN, 
                            SymbolVocabulary.PAD_TOKEN, 
                            SymbolVocabulary.EOS_TOKEN] + [c for c in string.printable]
        self.charmap = {v: i for i, v in enumerate(self.inv_charmap)}
        
    def __len__(self):
        return len(self.inv_charmap)
        
    def text2ids(self, text):
        [self.charmap[ch] if ch in self.charmap.keys() else UNK_IDX for ch in text] + [SymbolVocabulary.EOS_IDX]
        
    def ids2text(self, ids):
        return ''.join([self.inv_charmap[idx] for idx in idxs])
    
    
class ReducedSymbolVocabulary:
    UNK_IDX = 0
    UNK_TOKEN = '[UNK]'

    PAD_IDX = 1
    PAD_TOKEN = '[PAD]'

    EOS_IDX = 2
    EOS_TOKEN = '[EOS]'
    
    UP_IDX = 3
    UP_TOKEN = '[UP]'
    
    def __init__(self):
        self.inv_charmap = [ReducedSymbolVocabulary.UNK_TOKEN, 
                            ReducedSymbolVocabulary.PAD_TOKEN, 
                            ReducedSymbolVocabulary.EOS_TOKEN,
                            ReducedSymbolVocabulary.UP_TOKEN] + [c for c in (string.digits + 
                                                                      string.ascii_lowercase + 
                                                                      string.punctuation + 
                                                                      ' ')]
        self.charmap = {v: i for i, v in enumerate(self.inv_charmap)}
        
    def __len__(self):
        return len(self.inv_charmap)
        
    def text2ids(self, text):
        tokens = []
        for ch in text:
            if ch in self.charmap.keys():
                tokens.append(self.charmap[ch])
            elif ch in string.ascii_uppercase:
                tokens.append(ReducedSymbolVocabulary.UP_IDX)
                tokens.append(self.charmap[ch.lower()])
            else:
                tokens.append(ReducedSymbolVocabulary.UNK_IDX)
        tokens.append(ReducedSymbolVocabulary.EOS_IDX)
        return tokens
        
    def ids2text(self, ids, return_special=True):
        symbols = []
        i = 0
        while i < len(ids):
            if ids[i] == ReducedSymbolVocabulary.UP_IDX:
                if i < len(ids) - 1:
                    symbols.append(self.inv_charmap[ids[i + 1]].upper())
                    i += 2
                else:
                    if return_special:
                        symbols.append(ReducedSymbolVocabulary.UP_TOKEN)
                    i += 1
            else:
                if return_special or (ids[i] != ReducedSymbolVocabulary.EOS_IDX and
                                      ids[i] != ReducedSymbolVocabulary.PAD_IDX):
                    symbols.append(self.inv_charmap[ids[i]])
                i += 1
        return symbols